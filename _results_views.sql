SELECT *,
(SELECT department FROM _departments WHERE _departments.did=_exams.did) AS department,
(SELECT programme FROM _programmes WHERE _programmes.pid=_exams.pid) AS programme,
(SELECT fullname FROM _application WHERE _application.uid=_exams.uid) AS fullname,
(SELECT title FROM _courses WHERE _courses.`code`=_exams.`code` LIMIT 1) AS title,
(SELECT title FROM _courses WHERE _courses.`code`=_exams.`code` LIMIT 1) AS title,
(ca+exam) as total,
(SELECT grade FROM _gradingsystem WHERE total BETWEEN m1 AND m2) AS grade,
(SELECT point FROM _gradingsystem WHERE total BETWEEN m1 AND m2) AS point,
(SELECT point*_exams.cu FROM _gradingsystem WHERE total BETWEEN m1 AND m2) AS gp,
(SELECT remarks FROM _gradingsystem WHERE total BETWEEN m1 AND m2) AS remarks
FROM `_exams`