<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');
define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

require_once dirname(__FILE__) . '/Classes/PHPExcel.php';

$department = "ACCOUNTING";
$filename = $department.'.xlsx';
$inputFileType = 'Excel2007';
$objReader = PHPExcel_IOFactory::createReader($inputFileType);
$worksheetNames = $objReader->listWorksheetNames($filename);

echo '<table width="500" border="1" cellspacing="1" cellpadding="0">';
echo '<tr><td>id</td><td>sid</td><td>did</td><td>pid</td><td>sess</td><td>semester</td><td>uid</td><td>aid</td><td>rid</td><td>cid</td><td>cw</td><td>exam</td></tr>';

foreach ($worksheetNames as $sheetName) {
	$pid = $sheetName .' '.$department;
	echo "<tr><td>id</td><td>sid</td><td>did</td><td>$pid</td><td>sess</td><td>semester</td><td>uid</td><td>aid</td><td>rid</td><td>cid</td><td>cw</td><td>exam</td></tr>";
	//echo $sheetName .' '.$department, '<br />';
}

echo '</table>';
