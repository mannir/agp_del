<?php
ini_set('memory_limit', '-1');
include '../../../../default/settings.php'; global $url; global $cn;
$url= substr("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'],0,-10);

$cn=mysqli_connect(
		$databases['default']['default']['host'],
		$databases['default']['default']['username'],
		$databases['default']['default']['password'],
		$databases['default']['default']['database']
);

//$o = (object) mysqli_fetch_assoc(mysqli_query($cn,"SELECT * FROM _application WHERE uid='10'")); echo $o->fullname;
?>
<?php
require_once 'Classes/PHPExcel.php';

$deps = array('ACC','BUS','MAC','MEC','SLT',);

//$department = "MAC";
foreach ($deps as $k=>$v) {
	$filename = 'D:/Google Drive/AGP/agpresults/agpresults_2013_2014_sem1/'.$v.'.xlsx';
	$objReader = PHPExcel_IOFactory::createReader('Excel2007');
	$worksheetNames = $objReader->listWorksheetNames($filename);
	foreach ($worksheetNames as $k2=>$v2) {
		//echo $filename.'=='.$v2.'<br>';
		insert2db($cn, $filename, $k2);
	}
}


//print_r($arraydata);

function insert2db($cn, $filename, $sheetno) {
	$prg = array(
			'ACC_ND1_S1' => 'ACC111-4,GNS111-2,BFN111-2,OTM101-2,BAM111-2,BAM211-2,BFN122-3,BAM112-3,ICO131-2,OTM113-4,',
			'ACC_ND2_S1' => 'ACC211-3,ACC212-3,ACC213-3,ACC214-3,BAM216-2,BAM224-2,BAM213-2,BAM212-3,ISF211-2,GNS201-2,',
			'ACC_PRE-HND_S1' => 'ACC211-3,ACC212-3,ACC213-3,ACC214-3,BAM216-2,BAM224-2,BAM213-2,BAM212-3,ISF211-2,GNS201-2,',
			'ACC_HND1_S1' => 'ACC311-4,ACC312-4,ACC313-2,ACC315-3,ACC316-3,ACC317-2,HBF417-4,GNS301-4,ICO381-2,',
			'ACC_HND2_S1' => 'ACC411-4,ACC412-3,ACC413-3,ACC414-3,ACC415-4,ACC416-4,BAM418-3,OTM422-4,ISF411-2,',
			
			'BUS_ND1_S1' => 'BAM111,BAM112,BAM113,BAM114,BAM121,MKT111,PAD111,PUS111,GNS127,ICO108,GNS101,',
			'BUS_ND2_S1' => 'BAM211,BAM212,BAM213,BAM215,BAM214,BAM216,ACC214,BUS217,GNS201,GNS220,',
			'BUS_HND1_S1' => 'BAM301-2,BAM303-3,BAM305,BAM309-3,BAM311-3,GNS301-2,GNS312-2,ICO381-2,GNS127-2,GNS323-2,GNS332,',
			'BUS_HND2_S1' => 'BAM411,BAM412,BAM413,BAM405,BAM403,BAM414,BAM418,GNS401,',
			
			'MAC_ND1_S1' => 'GNS111-2,GNS101-2,MAC111-2,MAC112-2,MAC113-3,MAC114-3,MAC115-4,MAC116-2,MAC117-2,',
			'MAC_ND1_S2' => 'GNS102-2,GNS121-2,MAC121-2,MAC122-2,MAC123-3,MAC124-3,MAC125-2,MAC126-2,MAC127-3,BAM126-3,',
			'MAC_ND2_S1' => 'GNS201-2,GNS211-2,MAC211-2,MAC212-2,MAC213-3,MAC214-3,MAC215-4,MAC216-2,MAC217-2,BAM216,',
			'MAC_ND2_S2' => 'GNS201,GNS202-2,GNS225-2,MAC222-2,MAC223-2,MAC224-3,MAC225-3,MAC226-2,MAC227-2,MKT111-3,',
			'MAC_PRE-HND_S1' => 'GNS201-2,GNS211-2,MAC211-2,MAC212-2,MAC213-3,MAC214-3,MAC215-2,MAC216-2,MAC217-3,BAM216-3,',
			
			'MEC_ND1_S1' => 'COMP001-3,GNS101-2,GNS111-2,MTH112-3,EEC115-4,MEC111-4,MEC112,MEC113-5,',
			'MEC_ND2_S1' => 'BAM126-3,MTH202-2,EEC125-2,MEC211-5,MEC212-2,MEC213-4,MEC215-3,MEC217-2,',
			'MEC_ND2_S2' => 'COMP002-4,GNS202-2,MTH211-2,MEC123-4,EEC122-4,EEC123-4,EEC124-4,EEC125-3,',
			
			'SLT_ND1_S1' => 'STB111,STB112,STC111,STC112,STP111,STP112,STP114,GLT111,STC113,STP113,',
			'SLT_ND2_S1' => 'STB211-3,STB212-3,STM211-4,STC211-3,STC212-5,STP211-4,STP212-3,STP213-2,GNS211-2,COM215-5,RHS208-2,STM211-4,',
			'SLT_PRE-HND_S1' => 'STB212-3,STM211-3,STC211-4,STC212-3,STP211-5,STP212-4,STP213-2,GNS211-2,COM215-5,RHS208-2,',
			'BIOCHEMISTRY_HND1_S1' => 'GLT301-2,GLT303-3,STH311-3,STH312-2,STH313-2,COM301-2,GNS301-2,',
			'MICROBIOLOGY_HND1_S1' => 'GLT301-2,GLT302-2,GLT303-3,STM311-3,STM312-2,STH301-2,BAM216-2,COM123-2,GNS301-2,',
			'MICROBIOLOGY_HND2_S1' => 'STM411-2,STM412-2,STM413-3,STM414-3,STM415-2,STB421-3,GNS401-2,GNS413-3,',
	);
	$inputFileType = PHPExcel_IOFactory::identify($filename);
	$objReader = PHPExcel_IOFactory::createReader($inputFileType);
	$objReader->setReadDataOnly(true);
	$objPHPExcel = $objReader->load($filename);
	
	$total_sheets=$objPHPExcel->getSheetCount();
	$allSheetName=$objPHPExcel->getSheetNames();
	$objWorksheet = $objPHPExcel->setActiveSheetIndex($sheetno);
	$highestRow = $objWorksheet->getHighestRow();
	$highestColumn = $objWorksheet->getHighestColumn();
	$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
	$sheetname = $allSheetName[$sheetno];
	//$courses = "ACC111-4,GNS111-2,BFN111-2,OTM101-2,BAM111-2,BAM211-2,BFN122-3,BAM112-3,ICO131-2,OTM113-4,";
	echo '<table border="1" cellspacing="1" cellpadding="0">';
	echo '<tr><td>program</td><td>regno</td><td>fullname</td><td>courses</td><td>marks</td></tr>';
	
	
	for($row = 1; $row <= $highestRow;++$row) {
		$regno = $objWorksheet->getCellByColumnAndRow(0, $row)->getFormattedValue();
		$fullname = $objWorksheet->getCellByColumnAndRow(1, $row)->getFormattedValue();
		$courses = $prg[$sheetname];
		$marks = "";
	
		for ($col = 2; $col <$highestColumnIndex;++$col) {
			$value=$objWorksheet->getCellByColumnAndRow($col, $row)->getFormattedValue();
			$marks .= $value.',';
		}
		mysqli_query($cn,"INSERT INTO _results(program, regno, fullname, courses, marks) VALUES('$sheetname','$regno','$fullname','$courses','$marks')");
		/////echo "<tr><td>$sheetname</td><td>$regno</td><td>$fullname</td><td>$courses</td><td>$marks</td></tr>";
		//echo $regno.'=='.$marks.'<br>';
		/**
		 for ($col = 0; $col <$highestColumnIndex;++$col) {
			$value=$objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
			$arraydata[$row-1][$col]=$value;
	
			}
			*/
	}
	echo '</table>';
}


?>