<?php

$department = "ACCOUNTING";
$filename = '2014/'.$department.'.xlsx';

require_once 'Classes/PHPExcel.php';
$inputFileType = PHPExcel_IOFactory::identify($filename);
$objReader = PHPExcel_IOFactory::createReader($inputFileType);  
$objReader->setReadDataOnly(true);
$objPHPExcel = $objReader->load($filename);

$total_sheets=$objPHPExcel->getSheetCount(); 
$allSheetName=$objPHPExcel->getSheetNames(); 
$objWorksheet = $objPHPExcel->setActiveSheetIndex(1); 
$highestRow = $objWorksheet->getHighestRow(); 
$highestColumn = $objWorksheet->getHighestColumn();  
$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); 

echo '<table border="1" cellspacing="1" cellpadding="0">';
echo '<tr>';
for($i=1;$i<=$highestColumnIndex;$i++) {
	/////echo "<td>c$i</td>";
}
echo "<td>program</td>";
echo "<td>regno</td>";
echo "<td>fullname</td>";
echo "<td>courses</td>";
echo "<td>cu</td>";
echo '</tr>';
//echo '<tr><td>id</td><td>sid</td><td>did</td><td>pid</td><td>sess</td><td>semester</td><td>uid</td><td>aid</td><td>rid</td><td>cid</td><td>cw</td><td>exam</td></tr>';
$program = $allSheetName[0].' '.$department;
$regno = "";
$fullname = "";
$courses = "";
$marks = "";
for($row = 1; $row <= $highestRow;++$row) {
	
	
	$sn = $objWorksheet->getCellByColumnAndRow(0, $row)->getFormattedValue();
	if(preg_match("/^[0-9]+$/",$sn)) {
		$regno = $objWorksheet->getCellByColumnAndRow(1, $row)->getFormattedValue();
		$fullname = $objWorksheet->getCellByColumnAndRow(2, $row)->getFormattedValue();
	}
	//echo "<tr><td>$row</td><td>sid</td><td>did</td><td>pid</td><td>sess</td><td>semester</td><td>uid</td><td>aid</td><td>rid</td><td>cid</td><td>cw</td><td>exam</td></tr>";
	
	for ($col = 0; $col <$highestColumnIndex;++$col) {
		$value=$objWorksheet->getCellByColumnAndRow($col, $row)->getFormattedValue();
		//$arraydata[$row-1][$col]=$value;
		
		if (strpos($value, ':') !== FALSE) {
			$courses .=str_replace(' ','', substr($value, strpos($value, ":")+1)).',';
		}
		
		if($col==0 && preg_match("/^[0-9]+$/",$value)) {
			$c1 = $objWorksheet->getCellByColumnAndRow(3, $row)->getFormattedValue();
			$e1 = $objWorksheet->getCellByColumnAndRow(4, $row)->getFormattedValue();
			$c2 = $objWorksheet->getCellByColumnAndRow(6, $row)->getFormattedValue();
			$e2 = $objWorksheet->getCellByColumnAndRow(7, $row)->getFormattedValue();
			$c3 = $objWorksheet->getCellByColumnAndRow(9, $row)->getFormattedValue();
			$e3 = $objWorksheet->getCellByColumnAndRow(10, $row)->getFormattedValue();
			$mk = "";
			for($a=3;$a<$highestColumnIndex;$a++) {
				$mk .= $objWorksheet->getCellByColumnAndRow($a, $row)->getFormattedValue().'-'; //.'-'.
						//$objWorksheet->getCellByColumnAndRow($a+1, $row)->getFormattedValue();
			}
			$marks = $mk;
		}
		/**
		if(is_string($value) && strlen($value)>=10) {
			//
		}
		*/
		//if($col=0 && ctype_digit($value)) {
			//$cw = $objWorksheet->getCellByColumnAndRow($col, $row)->getFormattedValue();
			//$ex = $objWorksheet->getCellByColumnAndRow($col+1, $row)->getFormattedValue();
			//$marks .= $cw.'-'.$ex.',';
		//}
		//if(is_numeric($value)) { $marks.=$value.'-'; }
		///if (preg_match('#[0-9]#',$value)){ $marks.=$value.'-'; }
		
		
			
			/////echo "<td>$value</td>";
	
	}
	
	if($regno!='') {
		echo '<tr>';
	echo "<td>$program</td>";
	echo "<td>$regno</td>";
	echo "<td>$fullname</td>";
	echo "<td>$courses</td>";
	echo "<td>$marks</td>";
	echo '</tr>';
	}
	
	
}

echo '</table>';
//print_r($arraydata);
?>

<?php 
function code($str) {
	preg_match_all('!\d+!', $str, $matches);
	foreach($matches[0] as $k=>$v) {
		if(strlen($v)>3 && strlen($v)<10) { return $v; }
	} }
?>
