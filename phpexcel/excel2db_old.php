<?php
$department = "ACCOUNTING";
$filename = 'D:/Google Drive/AGP/agpresults/agpresults_2013_2014_sem1/'.$department.'.xlsx';

function totalsheets($file) {
	$arraydata = null;
	require_once 'Classes/PHPExcel.php';
	$inputFileType = PHPExcel_IOFactory::identify($file);
	$objReader = PHPExcel_IOFactory::createReader($inputFileType);
	$objReader->setReadDataOnly(true);
	$objPHPExcel = $objReader->load($file);
	$total_sheets=$objPHPExcel->getSheetCount();
	return $total_sheets;
}

function getData($file, $sheet) {
	$arraydata = null;
	require_once 'Classes/PHPExcel.php';
	$inputFileType = PHPExcel_IOFactory::identify($file);
	$objReader = PHPExcel_IOFactory::createReader($inputFileType);
	$objReader->setReadDataOnly(true);
	$objPHPExcel = $objReader->load($file);

	$total_sheets=$objPHPExcel->getSheetCount();
	$allSheetName=$objPHPExcel->getSheetNames();
	$objWorksheet = $objPHPExcel->setActiveSheetIndex($sheet);
	$highestRow = $objWorksheet->getHighestRow();
	$highestColumn = $objWorksheet->getHighestColumn();
	$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

	for($row = 1; $row <= $highestRow;++$row) {
		for ($col = 0; $col <$highestColumnIndex;++$col) {
			$value=$objWorksheet->getCellByColumnAndRow($col, $row)->getFormattedValue();
			$arraydata[$row-1][$col]=$value;

		}
	}

	return $arraydata;
}


$total = totalsheets($filename);

echo '<table border="1" cellspacing="1" cellpadding="0">';
echo '<tr><td>session</td><td>department</td><td>program</td><td>semester</td><td>regno</td><td>fullname</td><td>code</td><td>cu</td><td>ca</td><td>exam</td></tr>';

//for($i=0;$i<$total;$i++) {
	$data = getData($filename, 0);
	
	foreach ($data as $k1=>$v1) {
		$a1 = implode(",", $v1);
		$v = explode(",", $a1);
		if($v!='') {
			echo '<tr>';
			foreach ($v as $k2=>$v2) {
				echo "<td>$v2</td>";
			}
			echo '</tr>';
		//echo "<tr><td>session</td><td>department</td><td>program</td><td>semester</td><td>$v[1]</td><td>fullname</td><td>code</td><td>cu</td><td>ca</td><td>exam</td></tr>";
		}
		//echo "<tr><td>$a1</td></tr>";
		//echo $a1.'<br>';
	}
//}
echo '</table>';

