<?php // BISMILLAHIR RAHAMANIR RAHIM
error_reporting(0);
include 'mannir.php';

if(isset($_REQUEST['uid'])) $uid=$_REQUEST['uid'];
$ap = (object) mysqli_fetch_assoc(mysqli_query($cn, "SELECT * FROM _application WHERE uid='$uid' ORDER BY id DESC"));
$er = (object) mysqli_fetch_assoc(mysqli_query($cn, "SELECT * FROM _examresults WHERE uid=".$uid));

$o1 = (object) mysqli_fetch_assoc(mysqli_query($cn, "SELECT *,(SELECT department FROM _departments WHERE _departments.did=_programs.did) AS department,
(SELECT school FROM _schools WHERE _schools.sid=_programs.sid) AS school FROM `_programs` WHERE did=".$er->did));
//$o2 = (object) mysqli_fetch_assoc(mysqli_query($cn, "SELECT * FROM _programs WHERE pid=".$er->pid));
/**
$department = $o1->department;

$program = $o2->program;
$o3 = (object) mysqli_fetch_assoc(mysqli_query($cn, "SELECT * FROM `_schools` WHERE sid=".$o2->sid));
$school = $o3->school;
*/
$userid = $uid;
$regno = $er->regno;
$fullname = $ap->fullname;
$school = $o1->school;
$department = $o1->department;
$program = $o1->program;
$pincode = $ap->appno;
$tellerno = $ap->tellerno;
$session = $er->sess;
$date = $er->date;



require('fpdf/fpdf.php');
$pdf = new FPDF();		$pdf->SetFont('helvetica', '', 10);		$pdf->AddPage('P','A4');  $pdf->SetMargins(5,5);

//$pdf->Image('bg.png', '', 50, 200, 210, '', '', '', false, 300, '', false, false, 0);

$pdf->Image('logo.png',5,5,30,30);		


///$pdf->Image('mannir.jpg',172,5,30,30);
/**
	$p = (object) mysqli_fetch_assoc(mysqli_query($cn, "SELECT filename FROM file_managed WHERE fid=(SELECT picture FROM users WHERE uid='$uid')"));
	$filename = '../../../default/files/pictures/'.$p->filename;
	if (file_exists($filename)) { $pdf->Image($filename,172,5,30,30); }
*/
	$pdf->SetFont('Arial','B',20);		$pdf->Cell(190,0,"ABDU GUSAU POLYTECHNIC",0,0,'C'); $pdf->Ln(5);
	$pdf->SetFont('Arial','B',18); 	$pdf->Cell(200,5,"TALATA-MAFARA. ZAMFARA STATE",0,0,'C');$pdf->Ln(5);
	$pdf->SetFont('Arial','B',12); 	$pdf->Cell(200,5,"www.agp.edu.ng",0,0,'C');$pdf->Ln(8);
	$pdf->Cell(200,5,"ONLINE EXAMINATION RESULTS (2014/2015)",0,0,'C');$pdf->Ln(10);

	$pdf->SetFont('Arial','B',8); //$pdf->SetX(142);		

	$pdf->Cell(99,5, 'User ID: '.$userid,1,'','L');		$pdf->SetX(105);		$pdf->Cell(99,5, 'Registration No: '.$regno,1,'','L');		$pdf->Ln(6);
	$pdf->Cell(99,5, 'Fullname: '.$fullname,1,'','L');			$pdf->SetX(105);		$pdf->Cell(99,5, 'School: '.$school,1,'','L');	$pdf->Ln(6);
	$pdf->Cell(99,5, 'Department: '.$department,1,'','L');				$pdf->SetX(105);		$pdf->Cell(99,5, 'Programme: '.$program,1,'','L');	$pdf->Ln(6);
	$pdf->Cell(99,5, 'Pincode: '.$pincode,1,'','L');		$pdf->SetX(105);		$pdf->Cell(99,5, 'Teller No: '.$tellerno,1,'','L');	$pdf->Ln(6);
	//$pdf->Cell(99,5, 'Programme: '.$rg->programme,1,'','L');		$pdf->SetX(105);		$pdf->Cell(99,5, 'Session: '.$rg->session,1,'','L');	$pdf->Ln(6);
	$pdf->Cell(99,5, 'Session: '.$session,1,'','L');		$pdf->SetX(105);		$pdf->Cell(99,5, 'Date Register: '.$date,1,'','L');	$pdf->Ln(6);
	//$pdf->Cell(99,5, 'Session: '.$rg->session,1,'','C');				$pdf->SetX(105);		$pdf->Cell(99,5, 'User ID: '.$rg->regno,1,'','C');	$pdf->Ln(6);

	//$c1=explode(',',$rg->courses1); $c2=explode(',',$rg->courses2);

	//$pdf->SetX(60); $pdf->Cell(99,6, 'First Semester Courses',0,'','C');
	$pdf->Ln(10);
	
	$pdf->Cell(200,5,"FIRST SEMESTER RESULTS",0,0,'C');
	$pdf->Ln(5);
	$pdf->Cell(10,4, 'S/N',1,'','C');
	$pdf->Cell(20,4,'CODE',1,'','C');
	$pdf->Cell(70,4,'TITLE',1,'','C');
	$pdf->Cell(10,4, 'CU',1,'','C');
	$pdf->Cell(10,4, 'CA',1,'','C');
	$pdf->Cell(12,4, 'EXAM',1,'','C');
	$pdf->Cell(12,4, 'TOTAL',1,'','C');
	$pdf->Cell(12,4, 'GRADE',1,'','C');
	$pdf->Cell(12,4, 'POINTS',1,'','C');
	$pdf->Cell(10,4, 'GP',1,'','C');
	$pdf->Cell(22,4, 'REMARKS',1,'','C');
	$pdf->Ln(4);
	
	
	$cs1 = explode(",",$er->courses1);

	foreach ($cs1 as $k=>$v) {
		$cs = explode("-", $v);
		$code = $cs[0];
		$o2 = (object) mysqli_fetch_assoc(mysqli_query($cn, "SELECT code,title,cu FROM _courses WHERE code='$code'"));
		//exit($o2->title);
		//$title = $o2->title;
		//$cu = $o2->cu;
		$pdf->Cell(10,4, $k+1,1,'','C');
		$pdf->Cell(20,4, $cs[0],1,'','C');
		$pdf->Cell(70,4, $o2->title,1,'','C');
		$pdf->Cell(10,4, $cs[1],1,'','C');
		$pdf->Cell(10,4, $cs[2],1,'','C');
		$pdf->Cell(12,4, $cs[3],1,'','C');
		$pdf->Cell(12,4, $cs[2]+$cs[3],1,'','C');
		$pdf->Cell(12,4, grade($cs[2]+$cs[3]),1,'','C');
		$pdf->Cell(12,4, point($cs[2]+$cs[3]),1,'','C');
		$pdf->Cell(10,4, point($cs[2]+$cs[3])*$cs[1],1,'','C');
		if($cs[2]+$cs[3]>=40) { $pdf->Cell(22,4, 'PASSED',1,'','C'); }
		else { $pdf->Cell(22,4, 'C/O',1,'','C'); }
		$pdf->Ln(4);
	}
	
	$pdf->Ln(10);
	
	$pdf->Cell(200,5,"SECOND SEMESTER RESULTS",0,0,'C');$pdf->Ln(5);
	$pdf->Cell(10,4, 'S/N',1,'','C');
	$pdf->Cell(20,4,'CODE',1,'','C');
	$pdf->Cell(70,4,'TITLE',1,'','C');
	$pdf->Cell(10,4, 'CU',1,'','C');
	$pdf->Cell(10,4, 'CA',1,'','C');
	$pdf->Cell(12,4, 'EXAM',1,'','C');
	$pdf->Cell(12,4, 'TOTAL',1,'','C');
	$pdf->Cell(12,4, 'GRADE',1,'','C');
	$pdf->Cell(12,4, 'POINTS',1,'','C');
	$pdf->Cell(10,4, 'GP',1,'','C');
	$pdf->Cell(22,4, 'REMARKS',1,'','C');
	$pdf->Ln(4);
	$cs1 = explode(",",$er->courses2);
	
	foreach ($cs1 as $k=>$v) {
		
		$cs = explode("-", $v);
		$code = $cs[0];
		//$o2 = (object) mysqli_fetch_assoc(mysqli_query($cn, "SELECT code,title,cu FROM _courses WHERE code='".$v."'"));
		$o2 = (object) mysqli_fetch_assoc(mysqli_query($cn, "SELECT code,title,cu FROM _courses WHERE code='$code'"));
		//$title = $o2->title;
		//$cu = $o2->cu;
		$pdf->Cell(10,4, $k+1,1,'','C');
		$pdf->Cell(20,4, $cs[0],1,'','C');
		$pdf->Cell(70,4,  $o2->title,1,'','C');
		$pdf->Cell(10,4, $cs[1],1,'','C');
		$pdf->Cell(10,4, $cs[2],1,'','C');
		$pdf->Cell(12,4, $cs[3],1,'','C');
		$pdf->Cell(12,4, $cs[2]+$cs[3],1,'','C');
		$pdf->Cell(12,4, grade($cs[2]+$cs[3]),1,'','C');
		$pdf->Cell(12,4, point($cs[2]+$cs[3]),1,'','C');
		$pdf->Cell(10,4, point($cs[2]+$cs[3])*$cs[1],1,'','C');
		if($cs[2]+$cs[3]>=40) { $pdf->Cell(22,4, 'PASSED',1,'','C'); }
		else { $pdf->Cell(22,4, 'C/O',1,'','C'); }
		$pdf->Ln(4);
	}

	mysqli_close($cn);
	
	$x = $pdf->GetX();
	$y = $pdf->GetY();
	//$pdf->SetXY($x,$y+10);


	//exit($pdf->GetY());
	
	//$pdf->Ln(10);
	$pdf -> SetY(275);
	$pdf->SetFont('helvetica', '', 7); 
	$pdf->Cell('','', 'Copyright (c) 2014:- ABDU GUSAU POLYTECHNIC, TALATA-MAFARA ZAMFARA STATE (www.agp.edu.ng)',0,'','R');

$pdf->Output('ExamResults_'.$uid, 'I');


function grade($marks) {
	$rt = "";

	if ($marks < 40) { $rt = 'F'; }
	else if ($marks < 45) { $rt = 'E'; }
	else if ($marks < 50) { $rt = 'D'; }
	else if ($marks < 60) { $rt = 'C'; }
	else if ($marks < 70) { $rt = 'B'; }
	else if ($marks >= 70) {$rt = 'A'; }
	return $rt;
}

function point($marks) {
	$rt = "";

	if ($marks < 40) { $rt = 0; }
	else if ($marks < 45) { $rt = 1; }
	else if ($marks < 50) { $rt = 2; }
	else if ($marks < 60) { $rt = 3; }
	else if ($marks < 70) { $rt = 4; }
	else if ($marks >= 70) {$rt = 5; }
	return $rt;
}
?>