<?php
//define("SESSION", "2014/2015");
//error_reporting(0);
$session = "2014/2015";
global $cn;
//require('fpdf/fpdf.php');
require('ean13.php');
include '../../../default/settings.php';
$url= substr("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'],0,-10);
$cn=mysqli_connect(
		$databases['default']['default']['host'],
		$databases['default']['default']['username'],
		$databases['default']['default']['password'],
		$databases['default']['default']['database']
);
//$o = (object) mysqli_fetch_assoc(mysqli_query($cn,"SELECT * FROM _application WHERE uid='10'")); echo $o->fullname;
?>
<?php $uid=$_REQUEST['uid']; //error_reporting(0); //require_once('mannir_agp.inc'); 

session_start();	global $uid; $uid = $_REQUEST['uid']; // error_reporting(0);

if(isset($_REQUEST['uid'])) {
	$uid = $_REQUEST['uid'];
	$ap = (object) mysqli_fetch_assoc(mysqli_query($cn,"SELECT * FROM _application WHERE uid='$uid'"));
	
	//$pdf = new FPDF();
	$pdf=new PDF_EAN13();
	$pdf->SetFont('helvetica', '', 10);
	$pdf->AddPage('P','A4');
	$pdf->SetMargins(5,5);
	$pdf->Image('logo.png',5,5,25,25);
	
	//try {
	$url='/sites/default/files/pictures/'; if($_SERVER['HTTP_HOST']=='localhost'){ $url='agp\sites\default\files\pictures\\'; }
	$p = (object) mysqli_fetch_assoc(mysqli_query($cn,"SELECT filename FROM file_managed WHERE fid=(SELECT picture FROM users WHERE uid='$uid')"));
	if($p->filename) { $pdf->Image('../../../default/files/pictures/'.$p->filename,178,5,25,25); }
	
	$pdf->SetFont('Arial','B',18);		$pdf->Cell(190,0,"ABDU GUSAU POLYTECHNIC",0,0,'C'); $pdf->Ln(5);
	$pdf->SetFont('Arial','B',16); 		$pdf->Cell(200,5,"TALATA-MAFARA. ZAMFARA STATE",0,0,'C');$pdf->Ln(5);
	$pdf->SetFont('Arial','B',12); 		$pdf->Cell(200,5,"www.agp.edu.ng",0,0,'C');$pdf->Ln(8);
	$pdf->Cell(200,5,"ONLINE APPLICATION FORM $session",0,0,'C'); $pdf->Ln(10);
		
	$pdf->SetFont('times', 'B', 10);
	//$pdf->SetFont('Arial','B',8); //$pdf->SetX(142);
	$pdf->Cell(200,5,"PERSONAL INFORMATION",0,0,'C'); $pdf->Ln(5);
	$pdf->Cell(99,5, 'User ID: '.$ap->uid,1,'','L');		$pdf->SetX(105);		$pdf->Cell(99,5, 'Application No: '.$ap->appno,1,'','L');		$pdf->Ln(6);
	$pdf->Cell(99,5, 'Fullname: '.$ap->fullname,1,'','L');			$pdf->SetX(105);		$pdf->Cell(99,5, 'Gender: '.$ap->gender,1,'','L');	$pdf->Ln(6);
	$pdf->Cell(99,5, 'Date of Birth: '.$ap->dob,1,'','L');				$pdf->SetX(105);		$pdf->Cell(99,5, 'Place of Birth: '.$ap->placeofbirth,1,'','L');	$pdf->Ln(6);
	$pdf->Cell(99,5, 'Marital Status: '.$ap->maritalstatus,1,'','L');		$pdf->SetX(105);		$pdf->Cell(99,5, 'Blood Group: '.$ap->bloodgroup,1,'','L');	$pdf->Ln(6);
	$pdf->Cell(99,5, 'Address: '.$ap->address,1,'','L');		$pdf->SetX(105);		$pdf->Cell(99,5, 'Local Govt.: '.$ap->lga,1,'','L');	$pdf->Ln(6);
	$pdf->Cell(99,5, 'State: '.$ap->state,1,'','L');		$pdf->SetX(105);		$pdf->Cell(99,5, 'Country: '.$ap->country,1,'','L');	$pdf->Ln(6);
	$pdf->Cell(99,5, 'Mobile No: *****'.substr($ap->mobileno,5),1,'','L');		$pdf->SetX(105);		$pdf->Cell(99,5, 'Teller No: '.$ap->tellerno,1,'','L');	$pdf->Ln(6);
	$pdf->Cell(99,5, 'Higher Qualifcation: '.$ap->hq,1,'','L');		$pdf->SetX(105);		$pdf->Cell(99,5, 'Grade: '.$ap->gr,1,'','L');	$pdf->Ln(6);
	$pdf->Cell(99,5, 'Session: '.$ap->session,1,'','L');		$pdf->SetX(105);		$pdf->Cell(99,5, 'Date Applied: '.$ap->dateapp,1,'','L');
	$pdf->Ln(10);
	$pdf->Cell(200,5,"SCHOOL/ACADEMIC INFORMATION",0,0,'C'); $pdf->Ln(5);
	$pdf->Cell(5,5, 'SN',0,'','L');		$pdf->SetX(11);
	$pdf->Cell(85,5, 'NAME OF SCHOOL',0,'','L'); $pdf->SetX(97);
	$pdf->Cell(25,5, 'DATE FROM',0,'','L');		$pdf->SetX(123);
	$pdf->Cell(25,5, 'DATE TO',0,'','L');		$pdf->SetX(149);
	$pdf->Cell(55,5, 'CERTIFICATE',0,'','L');
	$pdf->Ln();
	
	$sch = explode(",", rtrim($ap->schoolinfo, ','));
	foreach ($sch as $k=>$v) {
		$s = explode(":", $v);
		$pdf->Cell(5,5, ($k+1).'.',1,'','L');		$pdf->SetX(11);
		$pdf->Cell(85,5, $s[0],1,'','L'); $pdf->SetX(97);
		$pdf->Cell(25,5, $s[1],1,'','L');		$pdf->SetX(123);
		$pdf->Cell(25,5, $s[2],1,'','L');		$pdf->SetX(149);
		$pdf->Cell(55,5, $s[3],1,'','L');
		$pdf->Ln(6);
	}		
	$pdf->Ln(5);	
	$pdf->Cell(200,5,"EXAMINATION INFORMATION",0,0,'C'); $pdf->Ln(5);
	$exm1 = explode(",", substr($ap->examinfo1,0,-1)); $e1=explode(':',$exm1[0]);
	$exm2 = explode(",", substr($ap->examinfo2,0,-1)); $e2=explode(':',$exm2[0]);
		
	$pdf->Cell(95,5, 'EXAM 1 = '.$exm1[0],1,'','L');			$pdf->SetX(109);		$pdf->Cell(95,5, 'EXAM 2 = '.$exm1[0],1,'','L');	$pdf->Ln(6);
	for($i=1;$i<=10;$i++) {
		$e1 = explode("=", $exm1[$i-1]);
		$e2 = explode("=", $exm2[$i-1]);
		if($i!=1) {
			$pdf->Cell(5,5, ($i-1).'.',1,'','L');		$pdf->SetX(11);
			$pdf->Cell(73,5, $e1[0],1,'','L');		$pdf->SetX(85);
			$pdf->Cell(15,5, $e1[1],1,'','L');		$pdf->SetX(109);
			
			$pdf->Cell(5,5, ($i-1).'.',1,'','L');		$pdf->SetX(115);
			$pdf->Cell(73,5, $e2[0],1,'','L');		$pdf->SetX(189);
			$pdf->Cell(15,5, $e2[1],1,'','L');
			$pdf->Ln(6);
		}
	}		
	$pdf->Ln(6);
	$pdf->Cell(200,5,"CONTACT INFORMATION",0,0,'C'); $pdf->Ln(5);
	$pdf->Cell(99,5, "Next of Kin Name: $ap->nkname",1,'','L');		$pdf->SetX(105);		$pdf->Cell(99,5, "Next of Kin Type: $ap->nktype",1,'','L');		$pdf->Ln(6);
	$pdf->Cell(99,5, "Next of Kin Address: $ap->nkaddress",1,'','L');			$pdf->SetX(105);		$pdf->Cell(99,5, "Next of Kin Mobile No: $ap->nkmobileno",1,'','L');	$pdf->Ln(6);
	$pdf->Cell(99,5, "Sponsor Name: $ap->spname",1,'','L');				$pdf->SetX(105);		$pdf->Cell(99,5, "Sponsor Type: $ap->sptype",1,'','L');	$pdf->Ln(6);
	$pdf->Ln(6);	
	$pdf->Cell(200,5,"APPLICATION INFORMATION",0,0,'C'); $pdf->Ln(5);
	$pdf->Cell(200,5, "First Choice Program: $ap->programme1",1,'','C'); $pdf->Ln(6);
	$pdf->Cell(200,5, "Second Choice Program: $ap->programme2",1,'','C'); $pdf->Ln(6);
	$pdf->Cell(200,5, "Third Choice Program: $ap->programme3",1,'','C');
	$pdf->Ln(10);
	$pdf -> SetY(-21);
	$pdf->SetFont('helvetica', '', 7);
	$pdf->Cell('','', 'Copyright (c) 2014:- ABDU GUSAU POLYTECHNIC, TALATA-MAFARA ZAMFARA STATE (www.agp.edu.ng)',0,'','R');
	$pdf->EAN13(10,270,$ap->appno);
	$pdf->Output('Application_Form_'.$uid, 'I');
}
?>