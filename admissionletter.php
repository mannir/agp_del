<?php error_reporting(0);	//require_once('mannir_agp.inc'); 
session_start(); global $uid;  $uid = $_REQUEST['uid']; $appno = $_REQUEST['appno'];

global $url; if($_SERVER['HTTP_HOST']=='localhost') { $cn=mysql_connect("localhost","root",""); mysql_select_db("agpapp");
$url='http://localhost/agpapp/sites/all/modules/manniragp/'; }
else { $cn=mysql_connect("50.63.244.172","agpapp","Mannir@13131"); mysql_select_db("agpapp");
global $url; $url='http://agpapp.esystems.me/sites/all/modules/manniragp/'; }

//$d = (object) mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM _admission WHERE uid='$uid'"));
//$ad = (object) mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM _admissions WHERE uid='$uid'"));
$ad = (object) mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM _admissions WHERE uid='$uid' OR appno='$appno'"));

require_once('tcpdf/config/lang/eng.php');
require_once('tcpdf/tcpdf.php');

class MYPDF extends TCPDF {
	public function Header() { global $uid; 
	$url='/sites/default/files/pictures/'; if($_SERVER['HTTP_HOST']=='localhost'){ $url='agp\sites\default\files\pictures\\'; }
		$p=mysqli_fetch_assoc(mysqli_query($con,"SELECT filename FROM file_managed WHERE fid=(SELECT picture FROM users WHERE uid='$uid')"));

		$this->Image('logo.jpg', 10, 10, 25, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
/**
		if($p!='') {
		$this->Image($_SERVER['DOCUMENT_ROOT'].$url.$p['filename'],175,10,25,25,'JPG','','R',false,300,'',false,false,0,false,0,false,false,false);
		}
		*/
		$this->SetFont('helvetica', 'B', 18);
		// Title
		$this->SetY(10); $this->Cell(0, 15, 'ABDU GUSAU POLYTECHNIC', 0, false, 'C', 0, '', 0, false, 'M', 'M');
		$this->SetY(16); $this->Cell(0, 15, 'TALATA MAFARA, ZAMFARA STATE', 0, false, 'C', 0, '', 0, false, 'M', 'M');
		$this->SetY(25); $this->Cell(0, 15, 'ONLINE ADMISSION LETTER', 0, false, 'C', 0, '', 0, false, 'M', 'M');

	}

	public function Footer() {
		$this->SetY(-10);
		$this->SetFont('helvetica', 'I', 8);
		$this->Cell(0, 10, 'ABDU GUSAU POLTECHNIC TALATA MAFARA, System Developed By Mannir eSystems Limited 2012 (www.mannir.net)', 0, false, 'C', 0, '', 0, false, 'T', 'M');
//		$this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
	}}

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('MUHAMMAD MANNIR AHMAD');
$pdf->SetTitle('ABDUGUSAU POLYTECHNIC TALATA MAFARA: Online Admission Letter');
$pdf->SetSubject('AGP Admission Letter');
$pdf->SetKeywords('Mannir, Mannir.com, eAdmission');
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetMargins(10, 10, 10);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->setLanguageArray($l);
$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');
$pdf->SetFont('times', 'B', 14);
$pdf->AddPage('P', 'A4');
$pdf->Image('bg.jpg', '', 50, 185, 210, '', '', '', false, 300, '', false, false, 0);

//$utf8text = file_get_contents('admissionletter.txt', false);
$admmsg = mysqli_fetch_assoc(mysqli_query($con,"SELECT value FROM variable WHERE name='manniragp_admmsg'")); $o = (object) $admmsg;
$utf8text = substr($o->value,7);

$pdf->SetY(40); $pdf->Cell(0, 0, 'Date: '.date('Y-M-d'), 0,'','R');
$pdf->SetY(55); $pdf->Cell(0,0,'MR./MISS/MRS.MAL./ALH.: '.$ad->fullname,0,1,'L');

$pdf->SetFont('helvetica', 'B', 20);

$pdf->SetY(65); $pdf->Cell(0,0,'PROVISIONAL OFFER OF ADMISSION TO READ',0,1,'C');
$pdf->SetY(75); $pdf->Cell(0,0,$ad->programme,0,1,'C');
$pdf->SetY(85); $pdf->Cell(0,0,'IN THE '.$ad->session.' ACADEMIC SESSION',0,1,'C');

$pdf->SetTextColor(0, 63, 127);
$pdf->SetY(100);
$pdf->SetFont('times', 'B', 18);
$pdf->Write(5, $utf8text, '', 0, '', false, 0, false, false, 0);

$pdf->SetY(240); $pdf->Cell(190, 0, '______________________', 0,'','C');
$pdf->SetY(250); $pdf->Cell(190, 0, 'For: REGISTRAR', 0,'','C');

//$pdf->SetY(255);
$pdf->SetXY(60, 255);
$style = array(
	'position' => '',
	'align' => 'C',
	'stretch' => false,
	'fitwidth' => true,
	'cellfitalign' => '',
	'border' => false,
	'hpadding' => 'auto',
	'vpadding' => 'auto',
	'fgcolor' => array(0,0,0),
	'bgcolor' => false, //array(255,255,255),
	'text' => false,
	'font' => 'helvetica',
	'fontsize' => 8,
	'stretchtext' => 4);
$pdf->write1DBarcode($uid.time(), 'C39E+', '', '', '', 15, 0.4, $style, 'N');

//$pdf->SetXY('', '');
$style = array(
    'border' => '',
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 1, // width of a single module in points
    'module_height' => 1 // height of a single module in points
);

//$pdf->Text(20, 25, 'QRCODE L');
//$pdf->write2DBarcode('www.kspdb.com', 'QRCODE,L', '', 27, 30, 30, $style, 'N');
//$pdf->write2DBarcode('SRD 019:MUHAMMAD MANNIR AHMAD:CHIEF LECTURER (DIR CSU):CENTRAL ADMIN', 'QRCODE,L', '55', 27, 30, 30, $style, 'N');

//$pdf->SetFont('helvetica', 'B', 14); $pdf->Cell(190, 0, 'The Admission Letter is not Valid if Re-Print or Edit!', 0,'','C');

$pdf->Output('Mannir_ePortal Admission Letter', 'I');